mocha.setup('bdd');
const assert = chai.assert;

/*
* Tests
*/

describe('createSmartObject', () => {
  it('создание объекта с "умной" функциональностью" с копированием исходных свойств', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    assert.equal(obj.name, 'Vasya');
    assert.equal(obj.surname, 'Ivanov');
    assert.equal(obj.patronymic, 'Olegovich');
  });

  // add new tests here
});

describe('defineComputedField', () => {
  it('добавление в объект "умного" поля', () => {
    const obj = createSmartObject({
      name: 'Vasya',
      surname: 'Ivanov',
      patronymic: 'Olegovich',
    });
    defineComputedField(
      obj,
      'fullName',
      data => `${data.name} ${data.surname} ${data.patronymic}`,
    );
    assert.equal(obj.fullName, 'Vasya Ivanov Olegovich');
  });

  // add new tests here
});

/*
* Run tests
*/

mocha.run();
